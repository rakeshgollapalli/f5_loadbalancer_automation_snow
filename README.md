# F5 load Balancer Ansible Automation with Service Now 

This is the Demo Code of ansible playbook calling each roles based on each task execution

This Playbook is integrating self service service-now form(RFC) with ansible for cheking Node/Pool Status and also enabling and disabling pool member.

Node_status.yml file: This is ansible code integrating with service-now for checking F5 node status and writing back output of task exection to the SNOW work notes of the tickect.

node.yml File: This is ansible code integrating with service-now for enabling and disabling node of F5 and writing back output of task exection of ansible to SNOW worknotes of the ticket and also close the ticket once task completed.

Pool_member.yml file: This is ansible code integrating with service-now for enabling and disabling active pool member  of F5 and writing back to SNOW worknotes of the ticket and also close the ticket once task completed.

pool_status.yml file: This is ansible code integrating with service-now for checking F5 pool status and writing back output of task exection to the SNOW work notes of the tickect.


